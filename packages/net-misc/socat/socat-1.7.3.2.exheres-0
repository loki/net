# Copyright 2008 Richard Brown
# Copyright 2011 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Multipurpose relay"
DESCRIPTION="
socat is a relay for bidirectional data transfer between two independent data
channels. Each of these data channels may be a file, pipe, device (serial line
etc. or a pseudo terminal), a socket (UNIX, IP4, IP6 - raw, UDP, TCP), an SSL
socket, proxy CONNECT connection, a file descriptor (stdin etc.), the GNU line
editor (readline), a program, or a combination of two of these. These modes
include generation of \"listening\" sockets, named pipes, and pseudo terminals.
"
HOMEPAGE="http://www.dest-unreach.org/${PN}/"
DOWNLOADS="http://www.dest-unreach.org/${PN}/download/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="readline tcpd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        readline? (
            sys-libs/ncurses
            sys-libs/readline:=
        )
        tcpd? ( sys-apps/tcp-wrappers )
"

# Upstream says test results are unreliable. Also see FAQ.
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/100-musl-compat.patch )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ip6
    --enable-openssl
    --disable-fips
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    readline
    "tcpd libwrap"
)
DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    BUGREPORTS \
    CHANGES \
    DEVELOPMENT \
    EXAMPLES \
    FILES \
    SECURITY \
    VERSION
)

src_install() {
    default

    docinto html
    dodoc doc/*.{css,html}
}

