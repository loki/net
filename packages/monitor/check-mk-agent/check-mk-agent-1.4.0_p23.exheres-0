# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="check_mk-${PV/_}"

require systemd-service [ systemd_files=[ cfg_examples/systemd/check_mk{.socket,@.service} ] ]

SUMMARY="Check_MK Agent for Linux"
DESCRIPTION="
The Check_MK Agent uses xinetd to provide information about the system on TCP port 6556. This can
be used to monitor the host via Check_MK.
"
HOMEPAGE="https://mathias-kettner.de/check_mk.html"
DOWNLOADS="https://mathias-kettner.de/download/${MY_PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="xinetd"

DEPENDENCIES="
    run:
        xinetd? ( sys-apps/xinetd )
    recommendation:
        dev-lang/python:*[<3] [[
            description = [ Required by some plugins written in Python ]
        ]]
"

WORK=${WORKBASE}/${MY_PNV}

src_unpack() {
    default

    edo pushd "${WORK}"
    edo tar xf agents.tar.gz
    edo popd
}

src_prepare() {
    # Errors when running under Python 3
    edo sed \
        -e 's:/usr/bin/python:/usr/bin/python2:' \
        -i plugins/{apache_status,isc_dhcpd,mk_inotify,mk_mongodb}

    default
}

src_test() {
    :
}

src_install() {
    newbin check_mk_agent.linux check_mk_agent
    newbin check_mk_caching_agent.linux check_mk_caching_agent
    dobin mk-job

    keepdir /etc/check_mk
    keepdir /usr/$(exhost --target)/lib/check_mk_agent/{,plugins,local,job,spool}

    install_systemd_files

    if option xinetd ; then
        insinto /etc/xinetd.d
        newins cfg_examples/xinetd.conf check_mk
    fi

    # Think about making the installation of the plugins configurable via SUBOPTIONS
    exeinto /usr/$(exhost --target)/lib/check_mk_agent/plugins
    doexe plugins/{apache_status,isc_dhcpd,lnx_quota,lvm,mk_filehandler,mk_inotify,mk_inventory.linux,mk_logins,netstat.linux,nfsexports,smart}
    doexe plugins/{mk_mongodb,mk_mysql,mk_oracle,mk_postgres}

    insinto /etc/check_mk
    doins cfg_examples/*.cfg
}

